---
title: "Ahmadou Dicko"
image: /images/profile.jpg
about:
  template: trestles
  links:
    - text: "{{< fa brands gitlab >}}"
      href: https://gitlab.com/dickoa
      rel: me
    - text: "{{< fa brands mastodon >}}"
      href: https://fosstodon.org/@dickoa
      rel: me
    - text: "{{< fa brands github >}}"
      href: https://github.com/dickoa
      rel: me
    - text: "{{< fa brands twitter >}}"
      href: https://twitter.com/dickoah
      rel: me
    - text: "{{< fa brands linkedin >}}"
      href: https://www.linkedin.com/in/ahmadoudicko
      rel: me
    - text: "{{< fa brands stack-overflow >}}"
      href: https://stackoverflow.com/users/592920/dickoa
      rel: me
comments: false
title-block-banner: true
---

<div>
I am a statistician and R enthusiast.
I believe in the use of data science for social good and have worked with several non-profit and research organizations.
</div>

## Education

- **_Ph.D._ in Economics**, **2016**
  *University of Dakar, Senegal*

- **_M.Sc._ in Statistics**, **2012**
  *ENSAE, Senegal*

## Experience

- **UNHCR** | Statistics and Data Analysis Officer | Jan 2021 - present

- **FAO** | Monitoring, evaluation and resilience analyst | June 2019 - October 2020

- **OCHA** | West and Central Africa Data team lead | Dec 2016 - June 2019

- **CIRAD** | Postdoctoral researcher | Jan 2016 - Dec 2016

- **Statistical consulting** | Statistician | Sept 2012 - Jan 2016
