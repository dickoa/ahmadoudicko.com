# ahmadoudicko.com

Repository for [ahmadoudicko.com](https://ahmadoudicko.com).

Website built with [Quarto](https://quarto.org) and content is under [CC-BY-NC-SA-4.0 License](https://creativecommons.org/licenses/by-nc-sa/4.0/).
