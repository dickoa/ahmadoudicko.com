---
title: "Fatal events in the Sahel between 2012 and 2018"
author:
  - name: Ahmadou Dicko
    url: https://fosstodon.org/@dickoa
date: "2019-02-27"
categories: [R, ACLED]
image: "feature.gif"
image-alt: "A map of sahel countries with the number of violent events animated between 2012 and 2018."
execute:
  message: false
  warning: false
  echo: true
---

In this short post, we will show how to use the `rhdx`, `racled`, `dplyr`, `purrr`, `sf` and `gganimate` R packages to show the number of fatal incidents in 5 Sahelian countries.
The [`rhdx`](https://gitlab.com/dickoa/rhdx) package is not yet on CRAN, so you will need to use the [`remotes`](https://cran.r-project.org/web/packages/remotes/index.html) package to install it first:

```{r}
#| eval: false
remotes::install_gitlab("dickoa/rhdx") ## github mirror also avalailable
```

The package `racled` will be used to pull conflict data from the [ACLED project](https://www.acleddata.com/). ACLED is a NGO specialized in conflict data collection and analysis.

```{r}
#| eval: false
remotes::install_gitlab("dickoa/racled") ## github mirror also avalailable
```

`gganimate` depends on the [`gifski`](https://cran.r-project.org/package=gifski) R package and to install it, make sure you first have [the `gifski` Rust cargo crate](https://gif.ski/) installed on your system.

```{r}
#| eval: false
install.packages("gifski")
```

This analysis was inspired by this tweet by José Luengo-Cabrera, researcher at the Crisis Group.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">G5 Sahel: conflict-related fatalities totaled 2,832 in 2018, a 74% increase relative to 2017.<br><br>- On average, 63% of fatalities have been concentrated in Mali since 2012.<br>- Last year, fatalities were largely located in central Mali, the Liptako-Gourma region &amp; the Lake Chad basin. <a href="https://t.co/feRtcxsScb">pic.twitter.com/feRtcxsScb</a></p>&mdash; José Luengo-Cabrera (@J_LuengoCabrera) <a href="https://twitter.com/J_LuengoCabrera/status/1100340244535263232?ref_src=twsrc%5Etfw">February 26, 2019</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Our visualization will be done for the following countries in the Sahel : _Burkina Faso_, _Mali_, _Chad_, _Mauritania_ and _Niger_.
There is a lot of insecurity and conflicts in these countries that resulted in the death of several thousands of people.

```{r}
library(tidyverse)
library(sf)
library(rhdx)
library(racled)
library(gganimate)
```

The goal of this post is to visualize the number of fatal events in theses countries between 2012 and 2018 using an animated map.
In order to do that, will need to get the administrative boundaries for these countries.
We can get the latest boundaries validated by the governments and the humanitarian community directly from [_HDX_](https://data.humdata.org/) using the `rhdx` package.
We will use `rhdx::pull_dataset` function and use the name of the dataset of interest as value. `rhdx::get_resource` and `rhdx::download_resource` allow to respectively get the a resource
by its index and read the data into memory.

```{r}
wca <- pull_dataset("west-and-central-africa-administrative-boundaries-levels") %>%
  get_resource(1) %>%
  read_resource()
glimpse(wca)
```

`wca` is a [`Simple Feature`](https://r-spatial.github.io/sf/articles/sf1.html) and we can manipulate it using `sf` and `dplyr`.
The data downloaded from _HDX_ covers the 24 countries of West and Central Africa, we will filter the data to extract the 5 countries of interest.

```{r}
g5_ab <- wca %>%
  filter(admin0Pcod %in% c("BF", "ML", "NE", "MR", "TD"))
```
We can now check our data by plotting it

```{r}
#| fig-alt: "G5 Sahel countries"
g5_ab %>%
  ggplot() +
  geom_sf() +
  theme_minimal()
```

Now that we have our background map, the next step is to get the _conflict data_. One of the main source for conflict data in the Sahel is [ACLED](https://www.acleddata.com/) and it can also be accessed using the `racled` package.

```{r}
#| cache: true
g5_acled_data <- read_acled(c("mali", "mauritania", "niger", "chad", "burkina faso"))
glimpse(g5_acled_data)
```

We have all the data we need for our analysis, we just need to aggregate total number fatalities by geographical coordinates, countries and year.

```{r}
g5_acled_fatalities_loc <- g5_acled_data %>%
  filter(year %in% 2012:2018, fatalities > 0) %>%
  group_by(year, country, latitude, longitude) %>%
  summarise(total_fatalities = sum(fatalities, na.rm = TRUE)) %>%
  arrange(year) %>%
  ungroup()
```

We can finally use our boundaries (`g5_ab`), the conflict data (`g5_acled_fatalities_loc`) with `gganimate` to dynamically visualize the different fatal incidents in G5 Sahel countries.

```{r anim_map}
#| eval: false
g5_ab %>%
  ggplot() +
  geom_sf(fill = "#383838", color = "gray") +
  coord_sf(datum = NA) +
  geom_point(data = g5_acled_fatalities_loc,
             aes(x = longitude,
                 y = latitude,
                 size = total_fatalities,
                 fill = total_fatalities),
             shape = 21,
             color = "transparent") +
  scale_fill_viridis_c(option = "plasma") +
  geom_sf_text(aes(label = admin0Name), color = "gray", fontface = "bold") +
  labs(x = "",
       y = "",
       title = "Fatal events in the Sahel G5",
       subtitle = "for the year {current_frame}",
       caption = "source: ACLED") +
  theme_void() +
  theme(legend.position = "none") +
  transition_manual(year, cumulative = TRUE) +
  shadow_mark()
```

```{r anim}
#| echo: false
#| out-width: "100%"
knitr::include_graphics("anim_map-1.gif")
```

Session info for this analysis.

<details>
<summary>Session info</summary>

```{r}
devtools::session_info()
```
</details>
